<?php
/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function theme_name_wp_title( $title, $sep ) {
    if ( is_feed() ) {
        return $title;
    }
    
    global $page, $paged;

    // Add the blog name
    $title .= " " . get_bloginfo( 'name', 'display' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description ";
    }

    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
    }

    return $title;
}
add_filter( 'wp_title', 'theme_name_wp_title', 10, 2 );

function my_scripts_method() {
    wp_enqueue_script(
        'functions',
        get_template_directory_uri() . '/js/functions.js',
        array( 'jquery' ),
        '1.0.0', 
        true
    );
    wp_enqueue_script(
        'slider',
        get_template_directory_uri() . '/slippry/dist/slippry.min.js'
    );
    wp_enqueue_style(
        'slidercss',
        get_template_directory_uri() . '/slippry/dist/slippry.css'
    );
}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

/**
 * Register our sidebars and widgetized areas.
 *
 */
function festival_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home bottom content',
		'id'            => 'home_bottom_1',
		'before_widget' => '<div class="home_bottom_content">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'festival_widgets_init' );

/**
 * Generate an excerpt of 50 words
 */
function gen_excerpt_50( $more ) {
	$excerpt = explode( ' ', get_the_excerpt() );
	$buildex = '';
	$cnt = 0;
	foreach( $excerpt as $word ) {
		if($cnt < 50) {
			$buildex .= $word . " ";
		}
		$cnt++;
	}
	$buildex .= ' ... <a class="fest_more_link" href="' . get_permalink( get_the_ID() ) . '">' . $more . '</a>';
	
	echo $buildex;
}

/**
 * Adds the Customize page to the WordPress admin area 
 */
function festival_customizer_menu() {
    add_theme_page( 'Festival Settings', 'Festival Settings', 'edit_theme_options', 'customize.php' );
}
add_action( 'admin_menu', 'festival_customizer_menu' );

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function festival_customizer( $wp_customize ) {
    $wp_customize->add_section(
        'festival_section_one',
        array(
            'title' => 'Festival Settings',
            'description' => 'This is a settings section.',
            'priority' => 35,
        )
    );
    
    //Register settings for customizer
    
    $wp_customize->add_setting(
        'calltoaction_textbox',
        array(
            'default' => '',
            'sanitize_callback' => 'festival_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'calltoaction_textbox',
        array(
            'label' => 'Call to action text',
            'section' => 'festival_section_one',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'calltoaction_page',
        array(
            'sanitize_callback' => 'festival_sanitize_integer',
        )
    );
     
    $wp_customize->add_control(
        'calltoaction_page',
        array(
            'type' => 'dropdown-pages',
            'label' => 'Choose a call to action page:',
            'section' => 'festival_section_one',
        )
    );
    
    $wp_customize->add_setting(
        'viewlist_textbox',
        array(
            'default' => '',
            'sanitize_callback' => 'festival_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'viewlist_textbox',
        array(
            'label' => 'View Events/Product/Category List',
            'section' => 'festival_section_one',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'listtype_textbox',
        array(
            'default' => '',
            'sanitize_callback' => 'festival_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'listtype_textbox',
        array(
            'label' => 'Post Type of List (product/event/page/etc.)',
            'section' => 'festival_section_one',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'listamount_textbox',
        array(
            'default' => '4',
            'sanitize_callback' => 'festival_sanitize_integer',
        )
    );
     
    $wp_customize->add_control(
        'listamount_textbox',
        array(
            'type' => 'text',
            'label' => 'Display List Amount:',
            'section' => 'festival_section_one',
        )
    );
    
    $wp_customize->add_setting(
        'fest_readmore_textbox',
        array(
            'default' => 'Buy Tickets',
            'sanitize_callback' => 'festival_sanitize_text',
        )
    );
     
    $wp_customize->add_control(
        'fest_readmore_textbox',
        array(
            'type' => 'text',
            'label' => 'Read More/Buy Tickets Excerpt Link:',
            'section' => 'festival_section_one',
        )
    );
    
    $wp_customize->add_setting(
        'viewlist_page',
        array(
            'sanitize_callback' => 'festival_sanitize_integer',
        )
    );
     
    $wp_customize->add_control(
        'viewlist_page',
        array(
            'type' => 'dropdown-pages',
            'label' => 'Choose a view list page:',
            'section' => 'festival_section_one',
        )
    );
    
    class Festival_Customize_Textarea_Control extends WP_Customize_Control {
        public $type = 'featured_textarea';
     
        public function render_content() {
            $allowed_html = array(
                'br' => array(),
                'span' => array()
            );
            ?>
                <label>
                    <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                    <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo wp_kses( $this->value(), $allowed_html ); ?></textarea>
                </label>
            <?php
        }
    }
    $wp_customize->add_setting( 'featured_textarea' );
     
    $wp_customize->add_control(
        new Festival_Customize_Textarea_Control(
            $wp_customize,
            'featured_textarea',
            array(
                'label' => 'Homepage Featured Text',
                'section' => 'festival_section_one',
                'settings' => 'featured_textarea'
            )
        )
    );
    
    $wp_customize->add_setting(
        'enable_slider',
        array(
            'sanitize_callback' => 'festival_sanitize_checkbox',
        )
    );
    $wp_customize->add_control(
        'enable_slider',
        array(
            'type' => 'checkbox',
            'label' => 'Enable Homepage Slider',
            'section' => 'festival_section_one',
        )
    );
    
    $wp_customize->add_setting(
        'copyright_textbox',
        array(
            'default' => '&copy; Copyright',
            'sanitize_callback' => 'festival_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'copyright_textbox',
        array(
            'label' => 'Copyright text',
            'section' => 'festival_section_one',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting(
        'hide_copyright',
        array(
            'sanitize_callback' => 'festival_sanitize_checkbox',
        )
    );
    $wp_customize->add_control(
        'hide_copyright',
        array(
            'type' => 'checkbox',
            'label' => 'Hide copyright text',
            'section' => 'festival_section_one',
        )
    );
    
    $wp_customize->add_setting(
        'link_color',
        array(
            'default' => '#4ba1cd',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_color',
            array(
                'label' => 'Link Color Setting',
                'section' => 'festival_section_one',
                'settings' => 'link_color',
            )
        )
    );
    
    $wp_customize->add_setting( 'logo_upload' );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'logo_upload',
            array(
                'label' => 'Logo Upload',
                'section' => 'festival_section_one',
                'settings' => 'logo_upload',
            )
        )
    );
    
    $wp_customize->add_setting(
        'bg_color',
        array(
            'default' => '#fcd76d',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bg_color',
            array(
                'label' => 'Background Color Setting',
                'section' => 'festival_section_one',
                'settings' => 'bg_color',
            )
        )
    );
    
    $wp_customize->add_setting( 'bg_upload' );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'bg_upload',
            array(
                'label' => 'Background Image Upload',
                'section' => 'festival_section_one',
                'settings' => 'bg_upload',
            )
        )
    );
    
    $wp_customize->add_setting( 'home_slide' );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'home_slide',
            array(
                'label' => 'Initial Homepage Slide Image Upload',
                'section' => 'festival_section_one',
                'settings' => 'home_slide',
            )
        )
    );
    $wp_customize->add_setting(
        'homeslide_link',
        array(
            'default' => '#',
            'sanitize_callback' => 'festival_sanitize_text',
        )
    );
    $wp_customize->add_control(
        'homeslide_link',
        array(
            'label' => 'Home slide link',
            'section' => 'festival_section_one',
            'type' => 'text',
        )
    );
	
    $wp_customize->add_setting( 'customjs_textarea' );
     
    $wp_customize->add_control(
        new Festival_Customize_Textarea_Control(
            $wp_customize,
            'customjs_textarea',
            array(
                'label' => 'Custom Javascript',
                'section' => 'festival_section_one',
                'settings' => 'customjs_textarea'
            )
        )
    );
}
add_action( 'customize_register', 'festival_customizer' );

//Santizers for customizer fields 

function festival_sanitize_text( $input ) {
	$allowed_html = array(
		'a' => array(
			'href' => array(),
			'title' => array()
		)
	);
    return wp_kses( $input , $allowed_html );
}

function festival_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

function festival_sanitize_integer( $input ) {
    if( is_numeric( $input ) ) {
        return intval( $input );
    }
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
function my_theme_wrapper_start() {
  echo '<section id="main">';
}

function my_theme_wrapper_end() {
  echo '</section>';
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

register_nav_menu( 'primary', __( 'Primary Menu', 'Festival' ) );