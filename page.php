<?php get_header(); ?>
<section id="main">
    <?php
        while ( have_posts() ) : the_post();
        ?>
        <div class="content">
            <h1><?php echo the_title(); ?></h1>
            <div <?php post_class(); ?>>
                <div class="entry clearfix">
                    <?php the_content(); ?>
					<br style='clear: both;' />
                </div>
            </div>
        </div>
    <?php endwhile; ?>
</section>
<?php get_footer(); ?>