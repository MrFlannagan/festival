<?php
/*
 *	Template Name: List Page
 */
 
 get_header(); ?>
<section id="main">
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="content">
			<h1><?php echo the_title(); ?></h1>
			<div <?php post_class(); ?>>
				<div class="entry clearfix">
					<?php the_content(); ?>
					<ul class="fest_listing">
					<?php
						$args = array(
							'post_type' => get_theme_mod( 'listtype_textbox' ),
							'product_cat' => 'event',
							'orderby' => 'menu_order',
							'order' => 'ASC',
							'posts_per_page' => 0
							);
						$loop = new WP_Query( $args );
						if ( $loop->have_posts() ) {
							while ( $loop->have_posts() ) : $loop->the_post();
								?><li>
								<?php 
								if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
									the_post_thumbnail( 'thumbnail' );
								}
								$date_value = get_post_meta( get_the_ID(), 'event_date', true );
								// Check if the custom field has a value.
								if ( ! empty( $date_value ) ) {
									echo '<p class="fest_event_date">' . $date_value . '</p>';
								}
								?>
								<a class='fest_listing_title_link' href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								<p><?php gen_excerpt_50( get_theme_mod( 'fest_readmore_textbox' ) ); ?></p>
								<div style='clear:both;'></div></li><?php
							endwhile;
						} else {
							echo __( 'No products found' );
						}
						wp_reset_postdata();
					?>
				</ul>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
</section>
<?php get_footer(); ?>