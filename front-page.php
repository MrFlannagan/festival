<?php get_header(); ?>
<div id="col1">
	<div id='slides'>
		<a href='<?php echo get_theme_mod( 'homeslide_link' ); ?>'><img src='<?php echo get_theme_mod( 'home_slide' ); ?>' border='0' /></a>
	</div>
	<div id='logo'>
		<img src='<?php echo get_theme_mod( 'logo_upload' ); ?>' />
	</div>
</div>
<div id="col2">
	<div class='tickets'>
		<a href='<?php echo site_url() . '?page_id=' . get_theme_mod( 'calltoaction_page' ); ?>'><?php echo get_theme_mod( 'calltoaction_textbox' ); ?></a>
	</div>
	<ul class="hp_products">
		<?php
			$args = array(
				'post_type' => get_theme_mod( 'listtype_textbox' ),
				'product_cat' => 'event',
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'posts_per_page' => get_theme_mod( 'listamount_textbox' )
				);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				while ( $loop->have_posts() ) : $loop->the_post();
					?><li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li><?php
				endwhile;
			} else {
				echo __( 'No products found' );
			}
			wp_reset_postdata();
		?>
		<li><a class='fest_listing_title_link' href="<?php echo site_url() . '?page_id=' . get_theme_mod( 'viewlist_page' ); ?>"><?php echo get_theme_mod( 'viewlist_textbox' ); ?></a></li>
	</ul>
	<div id='featured_textarea'>
		<?php echo str_replace( '\n', '<br />', get_theme_mod( 'featured_textarea' ) ); ?>
	</div>
</div>
<div id="col3">
	<div id="page_content">
		<?php 
			while ( have_posts() ) : the_post();
				the_content();
			endwhile;
		?>
	</div>
</div>
<br style="clear:both;" />
<?php if( get_theme_mod( 'enable_slider' ) != '') { ?>
<ul id="festival_slider">
<!--	<li><a href="#slide1"><img src="wp-content/themes/festival/slippry/demo/img/image-1.jpg" alt="This is caption 1 <a href='#link'>Even with links!</a>"></a></li> //-->
	<li><a href="#slide1"><img src="http://45.79.200.49/wp-content/uploads/2015/09/slide1.jpg"></a></li>
	<li><a href="#slide2"><img src="http://45.79.200.49/wp-content/uploads/2015/09/slide_new_4.jpg"></a></li>
	<li><a href="#slide3"><img src="http://45.79.200.49/wp-content/uploads/2015/09/slide2.jpg"></a></li>
	<li><a href="#slide3"><img src="http://45.79.200.49/wp-content/uploads/2015/09/slide1-2.jpg"></a></li>
	<li><a href="#slide3"><img src="http://45.79.200.49/wp-content/uploads/2015/09/slide1-4.jpg"></a></li>
	<li><a href="#slide3"><img src="http://45.79.200.49/wp-content/uploads/2015/09/slide_new_2.jpg"></a></li>
	<li><a href="#slide3"><img src="http://45.79.200.49/wp-content/uploads/2015/09/slide_new_3.jpg"></a></li>
</ul>
<script>
jQuery(function($) {
	$(function() {
		var festival_slider = $("#festival_slider").slippry({
			// transition: 'fade',
			// useCSS: true,
			// speed: 1000,
			// pause: 3000,
			// auto: true,
			// preload: 'visible',
			// autoHover: false
		});

		$('.stop').click(function () {
			festival_slider.stopAuto();
		});

		$('.start').click(function () {
			festival_slider.startAuto();
		});

		$('.prev').click(function () {
			festival_slider.goToPrevSlide();
			return false;
		});
		$('.next').click(function () {
			festival_slider.goToNextSlide();
			return false;
		});
		$('.reset').click(function () {
			festival_slider.destroySlider();
			return false;
		});
		$('.reload').click(function () {
			festival_slider.reloadSlider();
			return false;
		});
		$('.init').click(function () {
			festival_slider = $("#festival_slider").slippry();
			return false;
		});
	});
});
</script>
<?php 
	}
	if ( is_active_sidebar( 'home_bottom_1' ) ) { 
		dynamic_sidebar( 'home_bottom_1' );
	} 
	get_footer(); ?>