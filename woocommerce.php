<?php get_header(); ?>
<section id="main" class="woocommerce">
	<div class="content">
		<?php woocommerce_content(); ?>
	</div>
</section>
<?php get_footer(); ?>