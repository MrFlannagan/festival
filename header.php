<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
<title><?php wp_title( '' ); ?></title>
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
<?php wp_head(); ?>
</head>
<body>
<div id="header">
<div id="mobile-menu">
    <img src="<?php echo get_template_directory_uri() . '/img/mobile-nav.png'; ?>" />
</div>
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); 
	$menu_name = 'primary';
	$locations = get_nav_menu_locations();
	$menu_id = $locations[ $menu_name ] ;
	$my_menu = wp_get_nav_menu_object( $menu_id );
?>
<style type="text/css">
    #primary-menu li {
        width:<?php echo 100/$my_menu->count; ?>%;
    }
    body, #header {
        background-color:<?php echo get_theme_mod( 'bg_color' ); ?>;
    }
    body {
        background-image:url('<?php echo str_replace( 'http:', '//', get_theme_mod( 'bg_upload' ) ); ?>');
    }
</style>
</div>
<div id="wrapper">
<?php
    if ( !is_front_page() && !is_home() ) {
?>
    <div id="page-header">
        <a href="<?php echo site_url(); ?>"><img class="top-logo" src='<?php echo str_replace( 'http:', '//', get_theme_mod( 'logo_upload' ) ); ?>'  border='0' /></a>
        <div class="featured_textarea">
            <?php echo str_replace( '\n', '<br />', get_theme_mod( 'featured_textarea' ) ); ?>
        </div>
    </div>
<?php
    }
?>