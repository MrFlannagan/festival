<div id="footer">
<?php if( get_theme_mod( 'hide_copyright' ) == '') { ?>
    <?php esc_attr_e('©', 'responsive'); ?> <?php _e(date('Y')); ?> <a href="<?php echo home_url('/') ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><?php echo get_theme_mod( 'copyright_textbox', 'No copyright information has been saved yet.' ); ?></a>
<?php } // end if ?> - Website & Custom Wordpress Theme Developed By <a href='http://whoischris.com'>Chris Flannagan</a>
</div>
</div>
<?php wp_footer(); ?>
<script language='javascript' type='text/javascript'>
<?php echo get_theme_mod( 'customjs_textarea' ); ?>
</script>
</body>
</html>